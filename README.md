# Taxation-pajak

This is the project doing for online pajak. It is to compute personal taxable income based on income, married status and number of dependents.

## Run project

```sh
npm run start
```

## Run unit tests

```sh
npm run test
```
