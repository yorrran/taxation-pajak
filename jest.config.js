module.exports = {
  roots: ['<rootDir>/test'],
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'js'],
  testEnvironment: 'node',
};
