interface TaxRateCache {
  [key: string]: {
    previousLevel: number;
    fixedValue: number;
    taxRate: number;
  };
}

export default TaxRateCache;
