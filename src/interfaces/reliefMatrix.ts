interface ReliefMatrix {
  single: number;
  married: {
    dependent: {[key: string]: number};
  };
}

export default ReliefMatrix;
