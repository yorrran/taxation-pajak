import taxRateCache from './config/TaxRateCache';
import reliefMatrix from './config/ReliefMatrix';

const calculateTaxableIncome = (
  income: number,
  marriedStatus: boolean,
  dependents: number
) => {
  if (!marriedStatus && dependents > 0) {
    throw new Error('Error in married status and dependents');
  } else if (dependents > 4) {
    throw new Error('Error in number of dependents');
  }

  const relief = marriedStatus
    ? reliefMatrix.married.dependent[dependents.toString()]
    : reliefMatrix.single;

  return income * 12 - relief;
};

const computeTaxation = (taxableIncome: number) => {
  if (taxableIncome < 0) {
    throw new Error('invalid taxableIncome');
  }
  const levels = Object.keys(taxRateCache);

  for (let level of levels) {
    const limit = Number(level);
    if (limit.toString() !== 'NaN' && taxableIncome <= limit) {
      return (
        (taxableIncome - taxRateCache[level].previousLevel) *
          taxRateCache[level].taxRate +
        taxRateCache[level].fixedValue
      );
    }
  }

  return (
    taxableIncome -
    taxRateCache.overLimit.previousLevel * taxRateCache.overLimit.taxRate +
    taxRateCache.overLimit.fixedValue
  );
};

const income = 6500000;
const taxableIncome = calculateTaxableIncome(income, false, 0);
console.log('annual taxable income: ', computeTaxation(taxableIncome));

export {computeTaxation, calculateTaxableIncome};
