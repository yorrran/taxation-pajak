import TaxRateCache from '../interfaces/taxRateCache';

const taxRateCache: TaxRateCache = {
  50000000: {
    previousLevel: 0,
    fixedValue: 0,
    taxRate: 0.05,
  },
  250000000: {
    previousLevel: 50000000,
    fixedValue: 2500000,
    taxRate: 0.15,
  },
  500000000: {
    previousLevel: 250000000,
    fixedValue: 30250000,
    taxRate: 0.25,
  },
  overLimit: {
    previousLevel: 500000000,
    fixedValue: 45000000,
    taxRate: 0.3,
  },
};

export default taxRateCache;
