import ReliefMatrix from '../interfaces/reliefMatrix';

const reliefMatrix: ReliefMatrix = {
  single: 54000000,
  married: {
    dependent: {
      0: 58500000,
      1: 63000000,
      2: 67500000,
      3: 72000000,
    },
  },
};

export default reliefMatrix;
``;
