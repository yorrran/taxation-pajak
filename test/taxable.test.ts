import {computeTaxation, calculateTaxableIncome} from '../src';

describe('taxable income less than 0', () => {
  it('taxable income less than 0', () => {
    expect(() => computeTaxation(-1)).toThrowError('invalid taxableIncome');
  });
});

describe('taxable income in different ranges', () => {
  it('taxable income less than 50000000', () => {
    expect(computeTaxation(30000000)).toBe(1500000);
  });

  it('taxable income between 50000000 and 250000000', () => {
    expect(computeTaxation(25000000)).toBe(1250000);
  });

  it('taxable income between 250000000 and 500000000', () => {
    expect(computeTaxation(350000000)).toBe(55250000);
  });

  it('taxable income overlimit', () => {
    expect(computeTaxation(550000000)).toBe(445000000);
  });
});

describe('married status and dependents exception', () => {
  it('not married and more than 0 dependent', () => {
    expect(() => calculateTaxableIncome(50000000, false, 1)).toThrowError(
      'Error in married status and dependents'
    );
  });
  it('dependents is over limit', () => {
    expect(() => calculateTaxableIncome(50000000, true, 5)).toThrowError(
      'Error in number of dependents'
    );
  });
});

describe('married status and dependents to calculate taxable income', () => {
  it('married with 0 dependents', () => {
    expect(calculateTaxableIncome(55000000, true, 0)).toBe(601500000);
  });
  it('not married with 0 dependents', () => {
    expect(calculateTaxableIncome(55000000, false, 0)).toBe(606000000);
  });
});

describe('income is 6500000 and married with 1 child', () => {
  it('annual taxable income', () => {
    expect(calculateTaxableIncome(6500000, true, 1)).toBe(15000000);
  });
  it('annual tax income', () => {
    const annualTaxable = calculateTaxableIncome(6500000, true, 1);
    expect(computeTaxation(annualTaxable)).toBe(750000);
  });
});
